#ifndef NPC_H_
#define NPC_H_

#include "SDL2Common.h"
#include "Sprite.h"

// Forward declerations
// improve compile time. 
class Vector2f;
class Animation;
class Game;

class NPC : public Sprite
{
private:
    
    // Animation state
    int state;
        
    // Sprite information
    static const int SPRITE_HEIGHT = 64;
    static const int SPRITE_WIDTH = 32;

    // Need game
    Game* game;

    // weapon range
    float maxRange;
    float timeToTarget;

    //npc health
    int health;

public:
    NPC();
    ~NPC();

    // Player Animation states
    enum NPCState { LEFT = 0, RIGHT, UP, DOWN, IDLE, DEAD };
    
    void init(SDL_Renderer *renderer);
    void update(float timeDeltaInSeconds);

    // damage death and respawn
    void takeDamage(int damage); 
    bool isDead();
    void respawn(const int MAX_HEIGHT, const int MAX_WIDTH);
    // Update 'things' ai related
    
    void ai();
    void setGame(Game* game);

    int getCurrentAnimationState();

   


};
void NPC::init(SDL_Renderer* renderer)
{
    //path string
    string path("assets/images/undeadking-updated.png");

    //postion
    Vector2f position(300.0f, 300.0f);

    // Call sprite constructor
    Sprite::init(renderer, path, 6, &position);

    // Setup the animation objects
    animations[LEFT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1);
    animations[RIGHT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2);
    animations[UP]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3);
    animations[DOWN]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[DEAD]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 4);

    animations[DEAD]->setLoop(false);

    for (int i = 0; i < maxAnimations; i++) {
        animations[i]->setMaxFrameTime(0.4f);
    }
    aabb = new AABB(this->getPosition(), SPRITE_HEIGHT, SPRITE_WIDTH);
}
#endif