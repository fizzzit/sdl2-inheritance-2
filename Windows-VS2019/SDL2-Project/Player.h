#ifndef PLAYER_H_
#define PLAYER_H_

#include "SDL2Common.h"
#include "Sprite.h"

// Forward declerations
// improve compile time. 
class Vector2f;
class Animation;
class Game;

class Player : public Sprite
{
private:
    
    // Animation state
    int state;
    float cooldownTimer;
    static const float COOLDOWN_MAX;

    // Sprite information
    static const int SPRITE_HEIGHT = 32;
    static const int SPRITE_WIDTH = 24;

    Game* game;
public:
    Player();
    ~Player();

    // Player Animation states
    enum PlayerState{LEFT=0, RIGHT, UP, DOWN, IDLE};
    
    void init(SDL_Renderer *renderer);
    void processInput(const Uint8 *keyStates);


    void fire();

    void setGame(Game* game);
    void fire();

    void update(float timeDeltaInSeconds);

    int getCurrentAnimationState();
};

#endif